//
//  map.swift
//  Warcraft
//
//  Created by Alfredo Garcia on 10/11/16.
//  Copyright © 2016 Alex Yi. All rights reserved.
///Users/TheBatman/Documents/ECS160/WarCraft/ECS160iOS/Warcraft/MapViewController.swift

import UIKit

class map: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //arrayFromContentsOfFileWithName("Terrain")//will load all the contents into an array
        // Do any additional setup after loading the view.
    }
    /*
        The following function takes in a .dat file and loads into an array

    */
    func arrayFromContentsOfFileWithName(fileName: String) -> [String]? {
        guard let path = NSBundle.mainBundle().pathForResource(fileName, ofType: "dat") else {
            return nil
        }
        
        do {
            let content = try String(contentsOfFile:path, encoding: NSUTF8StringEncoding)
            return content.componentsSeparatedByString("\n")
        } catch _ as NSError {
            return nil
        }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func cropTile(image : UIImage,position : Int)->UIImage
    {
       // let contextImage: CGImagere = UIImage(CGImage: image.CGImage!)
        let rect  = CGRect(origin: CGPoint(x: 0,y: CGFloat(position) * 32), size: CGSize(width: 32,height: 32))      //  return CGImageCreateWithImageInRect(image, CGRect(origin: CGPoint(x: 0,y: CGFloat(position) * 32, 32,32)))
      //  let rect : GCRect  = CGRect(0,position*32,32,32)
      
        let croppedtile = CGImageCreateWithImageInRect(image.CGImage!, rect)
        return UIImage(CGImage: croppedtile!)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
