//
//  GraphicTileset.swift
//  Warcraft
//
//  Created by Alex Yi on 10/14/16.
//  Copyright © 2016 Alex Yi. All rights reserved.
//

import Foundation

class CGraphicTileset {
    
    //TODO: Figure out GdkPixbuf
    //GdkPixbuf *DPixbufTileset
    
    //TODO: Figure out GdkBitmap
    //std::vector< GdkBitmap * > DClippingMasks;
    var DClippingMasks = [ GdkBitmap ] ()
    
    //std::unordered_map< std::string, int > DMapping;
    var DMapping = [ String : Int ] ()
    
    //Number of tiles in the tileset
    var DTileCount: Int
    
    //Width of each tile
    var DTileWidth: Int
    
    //Height of each tile
    var DTileHeight: Int
    
    //Half of the width of each tile
    var DTileHalfWidth: Int
    
    //Half of the height of each tile
    var DTileHalfHeight: Int
    
    //The number of pixel colors
    var DPixelCount: Int
    
    //TODO: Figure out GdkColor
    //std::vector< GdkColor > DPixelColors;
    var DPixelColors = [ GdkColor ] ()
    
    //std::unordered_map< std::string, int > DPixelMapping;
    var DPixelMapping = [ String : Int ] ()
    
    func TileCount() -> Int {
        return DTileCount
    }
    
    func TileWidth() -> Int {
        return DTileWidth
    }
    
    func TileHeight() -> Int {
        return DTileHeight
    }
    
    func TileHalfWidth() -> Int {
        return DTileHalfWidth
    }
    
    func TileHalfHeight() -> Int {
        return DTileHalfHeight
    }
    
    func PixelCount() -> Int {
        return DPixelCount
    }
    
    //TOODO: Figure out GdkColor
//    func PixelColor(index: Int) -> GdkColor {
//        if ( (0 > index) || (DPixelColors.size() <= index) ) {
//            return GdkColor({0x000000, 0x0000, 0x0000, 0x0000});
//        }
//        return DPixelColors[index]
//    }
    
    //Constructor
    init() {
        //TODO: Figure out GdkPixbuf
//        DPixbufTileset = nil
        DTileCount = 0
        DTileWidth = 0
        DTileHeight = 0
        DTileHalfWidth = 0
        DTileHalfHeight = 0
        DPixelCount = 0
    }
    
    //TODO: Destructor?
    
    
    func TileCount(count: Int) -> Int {

//        GdkPixbuf *TempPixbufTileset;
//        guchar *Pixels;
//        guint PixelLength;
        
        if (0 > count) {
            return 0
        }
        
        if (!DTileWidth || !DTileHeight) {
            return 0
        }
        
        DTileCount = count
        
        if (count < DTileCount) {
            //TODO: I'm not 100% sure if the logic is right here
            for (key, value) in DMapping {
                
                if (value >= DTileCount) {
                    DMapping.removeValueForKey(key)
                }
            }
            
            return 1
            
        }
        
        //TempBixbufTileset = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, DTileWidth, count * DTileHeight);
        if( nil == TempPixbufTileset){
            return 0
        }
//        Pixels = gdk_pixbuf_get_pixels_with_length(TempPixbufTileset, &PixelLength);
//        memset(Pixels, 0, PixelLength)
        if(nil != DPixbufTileset) {
            var NumberChannels: Int
            NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset)
            
//            memcpy(Pixels, gdk_pixbuf_get_pixels(DPixbufTileset), NumberChannels * DTileHeight * DTileWidth * DTileCount);
//            g_object_unref(G_OBJECT(DPixbufTileset));
        }
        DPixbufTileset = TempPixbufTileset
        DTileCount = count
        return 1
    }
    
    func ClearTile(index: Int) -> Bool {
        var NumberChannels: Int, RowStride: Int
        
        if ((0 > index) || (index >= DTileCount)) {
            return false
        }
        
        if (nil == DPixbufTileset) {
            return false
        }
        
//        NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset);
//        RowStride = gdk_pixbuf_get_rowstride(DPixbufTileset);
//        
//        memset(gdk_pixbuf_get_pixels(DPixbufTileset) + (index * DTileHeight) * RowStride, 0, NumberChannels * DTileHeight * DTileWidth);
        
        return true
    }
    
    func DuplicateTile(destindex: Int, tilename: inout String, srcindx: Int) -> Bool {
        //guchar *Pixels;
        
        var NumberChannels: Int, RowStride: Int
        
        if ( (0 > srcindex) || (0 > destindex) || (srcindex >= DTileCount) || (destindex >= DTile)) {
            return false
        }
        if (tilename.isEmpty){
            return false
        }
        
        ClearTile(destindex)
        
//        NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset);
//        RowStride = gdk_pixbuf_get_rowstride(DPixbufTileset);
//        Pixels = gdk_pixbuf_get_pixels(DPixbufTileset);
//        
//        memcpy(Pixels + (destindex * DTileHeight) * RowStride, Pixels + (srcindex * DTileHeight) * RowStride, NumberChannels * DTileHeight * DTileWidth);
//        
        return true;
    }
    
    func OrAlphaTile(destindex: Int, srcindex: Int) -> Bool {
        
        //guchar *PixelSrc, *PixelDest;
        var NumberChannels, RowStride, TilePixels: Int
        
        if ( (0 > srcindex) || (0 > destindex) || (srcindex >= DTileCount) || (destindex >= DTileCount)) {
            
            return false
        }
        
        //NumberChannels = gdk_pixbuf_get_n_channels(DPixbufTileset);
        //RowStride = gdk_pixbuf_get_rowstride(DPixbufTileset);
        //PixelSrc = PixelDest = gdk_pixbuf_get_pixels(DPixbufTileset);
        
        PixelSrc += (srcindex * DTileHeight) * RowStride
        PixelDest += (destindex * DTileHeight) * RowStride
        TilePixels = DTileHeight * DTileWidth
        
        for TilePixel in TilePixels {
            if (0 == PixelSrc[3]) {
                PixelDest[3] = 0
            }
            
            PixelSrc += NumberChannels
            PixelDest += NumberChannels
        }
        
        return true
    }
    
    //From Charltons
    func FindTile( tilename: String ) -> Int {
        if ( DMapping[tilename] != nil ) {
            return DMapping[ tilename ]!
        }
        return -1
    }
    
    func FindPixel( tilename: String ) -> Int {
        if ( DPixelMapping[tilename] != nil ) {
            return DMapping[ tilename ]!
        }
        return -1
    }
    
    
    func FindPixel(pixelname: inout String) -> Int {
        
        //TODO: I'm not 100% sure if the logic is right here
        for DPixel in DPixelMapping {
            if let Iterator = DPixelMapping[pielname] {
                return Iterator
            } else {
                print("Continue")
            }
        }
        
        return -1
    }
    
    //TODO:
    //bool CGraphicTileset::LoadTileset(std::shared_ptr< CDataSource > source)
    
    //From Charltons file
    func DrawTile( gameScene: GameScene, typedDrawableidkwhatthisis: Int, Xpos: Int, Ypos: Int, TileNo: Int ) {
        let tile = SKSpriteNode( texture: mapAtlas[ TileNo ] )
        tile.position = CGPointMake( CGFloat( Xpos ), gameScene.size.height - CGFloat( Ypos ) )
        gameScene.addChild( tile )
    }
    
    //func DrawTileCorner
    
    //func DrawTileRectangle( gameScene: GameScene, typedDrawableidkwhatthisis: Int, Xpos: Int, Ypos: Int, Width: Int, Height: Int, TileNo: Int ) { }
    
    
    
    //From Charltons file
    func DrawClipped( gameScene: GameScene, Xpos: Int, Ypos: Int, TileNo: Int, color: Int){
        /*
         GdkColor ClipColor;
         
         ClipColor.pixel = color;
         ClipColor.red = (color>>8) & 0xFF00;
         ClipColor.green = color & 0xFF00;
         ClipColor.blue = (color<<8) & 0xFF00
         gdk_gc_set_clip_mask(gc, DClippingMasks[tileindex]);
         gdk_gc_set_clip_origin(gc, xpos, ypos);
         gdk_gc_set_rgb_fg_color(gc, &ClipColor);
         gdk_gc_set_rgb_bg_color(gc, &ClipColor);
         
         gdk_draw_rectangle(drawable, gc, TRUE, xpos, ypos, DTileWidth, DTileHeight);
         */
        DrawTile( gameScene, typedDrawableidkwhatthisis: typedDrawableidkwhatthisis , Xpos: Xpos , Ypos: Ypos , TileNo: TileNo )
    }
    
    func DrawPixel( gameScene: GameScene, typedDrawableidkwhatthisis: Int, Xpos: Int, Ypos: Int, size: Int, pixelIndex: Int) {
        
        if ((0 > pixelindex) || (pixelindex >= DPixelCount)) {
            return
        }
        if (0 >= size) {
            return
        }
        
        //gdk_gc_set_rgb_fg_color(gc, &DPixelColors[pixelindex]);
        //gdk_gc_set_rgb_bg_color(gc, &DPixelColors[pixelindex]);
        
        //gdk_draw_rectangle(drawable, gc, TRUE, xpos, ypos, size, size);
    }

    
    
}
