//
//  SelectMapViewController.swift
//  Warcraft
//
//  Created by Alex Yi on 10/11/16.
//  Copyright © 2016 Alex Yi. All rights reserved.
//

import UIKit
import AVFoundation

class SelectMapViewController: UIViewController {

    @IBOutlet weak var LongPress: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        var array:[String]
//      array = map().arrayFromContentsOfFileWithName("Terrain.dat")!
   //     LongPress.text = array[0]
        longPressGesture()
        
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    func longPressGesture() {
        
        let longPress = UILongPressGestureRecognizer(target: self, action: "longPressAction")
        longPress.minimumPressDuration = 2
        
        LongPress.addGestureRecognizer(longPress)
    }
    
    func longPressAction() {
        
        print("success")
        LongPress.text = "Text Changed"
        
    }

}