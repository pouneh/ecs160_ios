//
//  MapViewController.swift
//  Warcraft
//
//  Created by Alex Yi on 10/11/16.
//  Copyright © 2016 Alex Yi. All rights reserved.
//

import UIKit
import AVFoundation

class MapViewController: UIViewController {

    
    let image = UIImage(named: "Terrain.png")
   
    @IBOutlet var terrainTiles: [UIImageView]!
   
    @IBOutlet var tile: [UIImageView]!
    var audioPlayer = AVAudioPlayer()
    //var array = [UIImage]?()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("building-explode3", ofType: "wav")!)
        
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: sound, fileTypeHint: nil)
        } catch {
            
            print("error")
        }
        //audioPlayer.prepareToPlay()
        audioPlayer.play()

        
        for index in 0...292{
            
            terrainTiles[index].image = map().cropTile(image!, position: index)
            }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
