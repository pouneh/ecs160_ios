//
//  GameViewController.swift
//  Warcraft
//
//  Created by Alex Yi on 10/11/16.
//  Copyright (c) 2016 Alex Yi. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GameViewController: UIViewController {
    
    var midiPlayer:AVMIDIPlayer?
    var midiPlayerFromData:AVMIDIPlayer?
    
    var musicPlayer:MusicPlayer?
    var soundbank:NSURL?
    
    var musicSequence:MusicSequence!

    override func viewDidLoad() {
        super.viewDidLoad()

//        if let scene = GameScene(fileNamed:"GameScene") {
//            // Configure the view.
//            let skView = self.view as! SKView
//            skView.showsFPS = true
//            skView.showsNodeCount = true
//            
//            /* Sprite Kit applies additional optimizations to improve rendering performance */
//            skView.ignoresSiblingOrder = true
//            
//            /* Set the scale mode to scale to fit the window */
//            scene.scaleMode = .AspectFill
//            
//            skView.presentScene(scene)
//            
//            print("Hello")
//        }
        createAVMIDIPlayerFromMIDIFIle()
    }
    
    func createAVMIDIPlayerFromMIDIFIle() {
        
        guard let midiFileURL = NSBundle.mainBundle().URLForResource("menu", withExtension: "mid") else {
            fatalError("\"intro.mid\" file not found.")
        }
        guard let bankURL = NSBundle.mainBundle().URLForResource("generalsoundfont", withExtension: "sf2") else {
            fatalError("\"generalsoundfont.sf2\" file not found.")
        }
        
        do {
            try self.midiPlayer = AVMIDIPlayer(contentsOfURL: midiFileURL, soundBankURL: bankURL)
            print("created midi player with sound bank url \(bankURL)")
        } catch let error as NSError {
            print("Error \(error.localizedDescription)")
        }
        
        self.midiPlayer?.prepareToPlay()
        self.midiPlayer?.play(nil)
    }


    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
