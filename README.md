# ECS160iOS
ECS 160 Project iOS Version


Week 1

Current Goals

  * Point of Contact: Jeffrey Tai

  * Weekly Meeting Place and Time: TR 10 am - 12 pm Library Basement (subject to change)

  * Coding/Naming Convention:

      Indentation Style: Stroustrup Style

      Commenting Style: Single star aligned

      Project Convention: Follow Nitta’s

  * Tools/Language for Project:

      IDE: XCode 7.2

      Programming Language: Swift 2.2

      Framework: SpriteKit

#Planned User Interface
| Function | Gesture |
|:----|:----|
| Select unit | Single tap |
| Move unit | Double tap |
| Attack | Long press |
| Pan map | Double finger drag |
| Select group | Single finger drag |